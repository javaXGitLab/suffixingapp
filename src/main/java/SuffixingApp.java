import com.google.gson.*;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class SuffixingApp {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a path to desired directory");
        String path = scanner.nextLine();

        JSONReader jsonReader = new JSONReader(new File("C:\\Files\\Projects\\Maven\\src\\main\\resources\\suffix.json"));
        Suffix suffix = jsonReader.writeDataToObject(new Suffix());
        FileRenamer renamer = new FileRenamer(path);
        renamer.renameFiles(suffix);

    }
}
