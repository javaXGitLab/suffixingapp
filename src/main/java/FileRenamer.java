import java.io.File;
import java.util.Scanner;

public class FileRenamer {

    String path;

FileRenamer(String path){
    this.path=path;

}

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void renameFiles(Suffix suffix){


        File folder = new File(path);

        File[] files = folder.listFiles();

        for (File f: files) {

            File renamed = new File(f.getPath().replaceAll("old", suffix.getSuffix()));
            if (f.renameTo(renamed)){
                System.out.println(renamed.getName() +" Renamed Successfully");
            }else {
                System.out.println(renamed.getName() +" Doesn't Renamed");
            }

        }
    }

}
