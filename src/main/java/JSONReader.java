import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JSONReader {

    private String jsonData;

    public JSONReader(File jsonFile) throws IOException {
        jsonData = FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8);
    }


    public Suffix writeDataToObject(Suffix suffix) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        suffix = gson.fromJson(jsonData,Suffix.class);
        return suffix;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }
}
