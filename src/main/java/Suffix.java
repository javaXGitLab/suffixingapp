public class Suffix {
    private String suffix;

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return "Suffix{" +
                "suffix='" + suffix + '\'' +
                '}';
    }
}
